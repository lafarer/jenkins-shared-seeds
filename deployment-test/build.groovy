
def gitUrl = 'git@bitbucket.org:porphyrio/deployment-test.git'
def gitCredentials = 'bitbucket_key_id_rsa'
def gitBranches = 'origin/develop'

folder('deployment-test') {
    displayName('deployment-test')
    description('deployment-test jobs')
}

pipelineJob('deployment-test/build-develop') {
    //displayName("BUILD - develop")
    description("Builds deployment-test from develop branch.")
    concurrentBuild(false)
    quietPeriod(1)
    logRotator {
        numToKeep(10)
    }
    triggers {
        // Twice a day
        cron('H 0,12 * * *')
        // Every 5 minutes
        scm('H/5 * * * *')
    }
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        credentials(gitCredentials)
                        url(gitUrl)
                    }
                    branches(gitBranches)
                    extensions {
                        wipeOutWorkspace()
                        localBranch()
                    }
                }
            }
            lightweight(true)
            scriptPath('Jenkinsfile')
        }
    }
}
